# Geofinder

## Usage

```shell
# Print help
python -m geofinder -h

# Find cities in the world whose names end in 'olu'
python -m geofinder --suffix olu

# Find cities in France whose names end in 'rac'
python -m geofinder --suffix --country FR rac

# Find cities in France whose names begin with 'can'
python -m geofinder --prefix --country FR can
```

## Features

- Find cities names by suffix or prefix
- Filter by country
- Display on map

## Resources

- [StackOverflow - Plot latitude longitude from CSV](https://stackoverflow.com/a/74105923)
