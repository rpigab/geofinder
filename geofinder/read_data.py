import pandas as pd


def read_tsv_to_dataframe():
    # Reading CSV data into a pandas DataFrame
    df = pd.read_csv(INPUT_DATA, sep='\t', header=None, names=COLUMN_TYPES, dtype=COLUMN_TYPES)

    # Remove rows with missing values in the asciiname column
    df = df.dropna(subset=['asciiname'])

    return df


COLUMN_TYPES = {
    # integer id of record in geonames database
    'geonameid': int,
    # name of geographical point (utf8) varchar(200)
    'name': str,
    # name of geographical point in plain ascii characters, varchar(200)
    'asciiname': str,
    # alternatenames, comma separated, ascii names automatically transliterated,
    # convenience attribute from alternatename table, varchar(10000)
    'alternatenames': str,
    # latitude in decimal degrees (wgs84)
    'latitude': float,
    # longitude in decimal degrees (wgs84)
    'longitude': float,
    # see http://www.geonames.org/export/codes.html, char(1)
    'feature class': str,
    # see http://www.geonames.org/export/codes.html, varchar(10)
    'feature code': str,
    # ISO-3166 2-letter country code, 2 characters
    'country code': str,
    # alternate country codes, comma separated, ISO-3166 2-letter country code, 200 characters
    'cc2': str,
    # fipscode (subject to change to iso code), see exceptions below,
    # see file admin1Codes.txt for display names of this code; varchar(20)
    'admin1 code': str,
    # code for the second administrative division, a county in the US,
    # see file admin2Codes.txt; varchar(80)
    'admin2 code': str,
    # code for third level administrative division, varchar(20)
    'admin3 code': str,
    # code for fourth level administrative division, varchar(20)
    'admin4 code': str,
    # bigint (8 byte int)
    'population': int,
    # in meters, integer
    # Or N/A
    'elevation': str,
    # digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m)
    # or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
    'dem': int,
    # the iana timezone id (see file timeZone.txt) varchar(40)
    'timezone': str,
    # date of last modification in yyyy-MM-dd format
    'modification date': str,
}

INPUT_DATA = './data/cities500.txt'

if __name__ == '__main__':
    read_tsv_to_dataframe()
