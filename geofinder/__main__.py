import logging

from geofinder.args import parse
from geofinder.filters import filter_by_2char_countrycode, filter_by_asciiname_suffix, filter_by_asciiname_prefix
from geofinder.geonames import download
from geofinder.plot import plot
from geofinder.read_data import read_tsv_to_dataframe
from geofinder.setup_logging import setup_logging

setup_logging()

# parse cmdline args
args = parse()

# download data if needed
logging.info('download data')
download()

# Find cities in data by prefix or suffix
df = read_tsv_to_dataframe()

if args.country:
    df = filter_by_2char_countrycode(df, args.country)

if args.prefix:
    logging.info(f'filtering by prefix [{args.pattern}]')
    df = filter_by_asciiname_prefix(df, args.pattern)
elif args.suffix:
    logging.info(f'filtering by suffix [{args.pattern}]')
    df = filter_by_asciiname_suffix(df, args.pattern)
else:
    logging.error('option prefix or suffix not found')
    exit(1)

# Plot to map
plot(df)
