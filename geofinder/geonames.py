"""
Get cities data from https://www.geonames.org/
"""
import logging
import os
import requests
from zipfile import ZipFile

CITIES_500_ZIP_URI = 'https://download.geonames.org/export/dump/cities500.zip'
DATA_FOLDER = './data'
DATA_FILE_NAME = 'cities500.txt'


def download():
    # Don't download if data already exists
    if os.path.exists(os.path.join(DATA_FOLDER, DATA_FILE_NAME)):
        logging.debug(f'nothing to download, {DATA_FILE_NAME} exists in {DATA_FOLDER}')
        return

    # Create a 'data' folder if it doesn't exist
    if not os.path.exists(DATA_FOLDER):
        raise FileNotFoundError('data folder must exist')

    try:
        # Download the zip file
        response = requests.get(CITIES_500_ZIP_URI)
        response.raise_for_status()

        # Save the zip file in the 'data' folder
        zip_file_path = os.path.join(DATA_FOLDER, 'cities500.zip')
        with open(zip_file_path, 'wb') as zip_file:
            zip_file.write(response.content)

        # Extract the contents of the zip file
        with ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extract('cities500.txt', DATA_FOLDER)

        logging.info('download and extraction completed successfully.')

    except requests.exceptions.RequestException as e:
        logging.error(f'frror downloading the file: {e}')


if __name__ == '__main__':
    download()
