import plotly.express as px


def plot(df):
    color_scale = [(0, 'orange'), (1, 'red')]

    fig = px.scatter_mapbox(df,
                            lat='latitude',
                            lon='longitude',
                            hover_name='name',
                            hover_data=['name', 'population'],
                            color_continuous_scale=color_scale,
                            color='population',
                            size='population',
                            zoom=8,
                            height=800,
                            width=800)

    fig.update_layout(mapbox_style='open-street-map')
    fig.update_layout(margin={'r': 0, 't': 0, 'l': 0, 'b': 0})
    fig.show()
