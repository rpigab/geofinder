def filter_by_asciiname_prefix(df, prefix):
    return df[df['asciiname'].str.contains(f'^{prefix}', case=False)]


def filter_by_asciiname_suffix(df, suffix):
    return df[df['asciiname'].str.contains(f'{suffix}$', case=False)]


def filter_by_2char_countrycode(df, iso2_countrycode: str):
    return df[df['country code'] == iso2_countrycode.upper()]
