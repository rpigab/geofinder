import argparse
import logging


def parse():
    parser = argparse.ArgumentParser(description='Process command line arguments')

    parser.add_argument('pattern', type=str, help='Mandatory pattern to look for in city name')
    parser.add_argument('--prefix', action='store_true',
                        help='Activate if <pattern> is a prefix of city name to look for')
    parser.add_argument('--suffix', action='store_true',
                        help='Activate if <pattern> is a suffix of city name to look for')
    parser.add_argument('--country', type=str, help='ISO 2 char country code to filter results')

    args = parser.parse_args()
    logging.debug(args)
    return args


if __name__ == '__main__':
    parse()
